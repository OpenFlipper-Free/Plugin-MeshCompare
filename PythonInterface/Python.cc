
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <MeshComparePlugin.hh>
#include <QString>
#include <QChar>

#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(MeshCompare, m) {

  QObject* pluginPointer = getPluginPointer("MeshCompare");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-MeshCompare" << std::endl;
     return;
   }

  MeshComparePlugin* plugin = qobject_cast<MeshComparePlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-MeshCompare" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< MeshComparePlugin,std::unique_ptr<MeshComparePlugin, py::nodelete> > compare(m, "MeshCompare");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  compare.def(py::init([plugin]() { return plugin; }));

  compare.def("compare", &MeshComparePlugin::compare,
                                 QCoreApplication::translate("PythonDocMeshCompare","Compares two meshes. Use the getter functions to retrieve the maximal deviations").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshCompare","ID of the first object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshCompare","ID of the second object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshCompare","Compute distance between meshes").toLatin1().data()) = true,
                         py::arg(QCoreApplication::translate("PythonDocMeshCompare","Compute normal deviation between meshes").toLatin1().data()) = true,
                         py::arg(QCoreApplication::translate("PythonDocMeshCompare","Compute Gauss curvature deviation between meshes").toLatin1().data()) = true,
                         py::arg(QCoreApplication::translate("PythonDocMeshCompare","Compute mean curvature deviation between meshes").toLatin1().data()) = true,
                         py::arg(QCoreApplication::translate("PythonDocMeshCompare","Compare only selected vertices").toLatin1().data()) = false);

  compare.def("lastMaximalDistance", &MeshComparePlugin::lastMaximalDistance,
                                 QCoreApplication::translate("PythonDocMeshCompare","Get the maximal distance of the last comparison (-1, if no comparison performed so far)").toLatin1().data() );

  compare.def("lastMaximalNormalDeviation", &MeshComparePlugin::lastMaximalNormalDeviation,
                                  QCoreApplication::translate("PythonDocMeshCompare","Get the maximal normal deviation of the last comparison in degree (-1, if no comparison performed so far)").toLatin1().data() );

  compare.def("lastMaximalMeanCurvatureDeviation", &MeshComparePlugin::lastMaximalMeanCurvatureDeviation,
                                  QCoreApplication::translate("PythonDocMeshCompare","Get the maximal mean curvature deviation of the last comparison (-1, if no comparison performed so far)").toLatin1().data() );

  compare.def("lastMaximalGaussCurvatureDeviation", &MeshComparePlugin::lastMaximalGaussCurvatureDeviation,
                                  QCoreApplication::translate("PythonDocMeshCompare","Get the maximal Gauss curvature deviation of the last comparison (-1, if no comparison performed so far)").toLatin1().data() );

}
