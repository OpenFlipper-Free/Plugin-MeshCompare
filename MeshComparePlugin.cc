/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/





#include "MeshComparePlugin.hh"


#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

#include <ACG/Utils/ColorCoder.hh>

#include <ACG/Scenegraph/MaterialNode.hh>
#include <ACG/QtScenegraph/QtTranslationManipulatorNode.hh>

//--------------------------------------------------------------------------------------------


MeshComparePlugin::MeshComparePlugin() :
  tool_(nullptr),
  toolIcon_(nullptr),
  maximalDistance_(-1),
  maxNormalDeviation_(-1),
  maxMeanCurvatureDev_(-1),
  maxGaussCurvatureDev_(-1),
  plot_(0)
{

}

MeshComparePlugin::~MeshComparePlugin()
{
   delete toolIcon_;
}

void MeshComparePlugin::initializePlugin()
{
  if ( OpenFlipper::Options::gui()) {
    tool_ = new MeshCompareToolbarWidget();

    connect( tool_->compare, SIGNAL(clicked()), this, SLOT(compareButton()) );
    connect( tool_->clear, SIGNAL(clicked()), this, SLOT(slotClear()) );

    toolIcon_ = new QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"MeshCompare.png");
    emit addToolbox( tr("Mesh Compare") , tool_ , toolIcon_);

    connect(tool_->doClamp,SIGNAL(toggled( bool)),this,SLOT(slotClampBox(bool)));

    QVBoxLayout* layout = new QVBoxLayout(tool_->frame);

    plot_ = new QChartsPlot(tool_);
    plot_->setMinimumHeight(300);

    layout->addWidget(plot_);
  }
}

bool MeshComparePlugin::eventFilter(QObject *obj, QEvent *event) {
  if (event->type() != QEvent::Move) {
    std::cout << "Event: " << event->type() << std::endl;
  }
  return false;
}

void MeshComparePlugin::pluginsInitialized() {
    
  //===========================================================
  // Describe scripting slots
  //===========================================================
  emit setSlotDescription(tr("compare(int,int)"), tr("Compare two meshes. Use lastMaximalDistance() and lastMaximalNormalDeviation() to get the results."),
      QStringList(tr("ObjectId,ObjectId")), QStringList(tr("Id of the reference mesh, Id of the comparison mesh")));
  emit setSlotDescription(tr("lastMaximalDistance()"), tr("Get the maximal distance between the meshes of the last comparison."),
      QStringList(tr("")), QStringList(tr("")));
  emit setSlotDescription(tr("lastMaximalNormalDeviation()"), tr("Get the maximal normal deviation in degree between the meshes of the last comparison."),
      QStringList(tr("")), QStringList(tr("")));
  emit setSlotDescription(tr("lastMaximalMeanCurvatureDeviation()"), tr("Get the maximal mean curvature deviation between the meshes of the last comparison."),
      QStringList(tr("")), QStringList(tr("")));
  emit setSlotDescription(tr("lastMaximalGaussCurvatureDeviation()"), tr("Get the maximal gauss curvature deviation between the meshes of the last comparison."),
      QStringList(tr("")), QStringList(tr("")));

  //===========================================================
  // Check mean curvature plugin and disable the box in gui mode
  //===========================================================
  bool meanCurvature = false;
  emit pluginExists( "meancurvature" , meanCurvature  );

  if ( OpenFlipper::Options::gui() && !meanCurvature )
    tool_->meanCurvature->setEnabled(false);

  //===========================================================
  // Check gauss curvature plugin and disable the box in gui mode
  //===========================================================
  bool gaussCurvature = false;
  emit pluginExists( "gausscurvature" , gaussCurvature  );

  if ( OpenFlipper::Options::gui() && !gaussCurvature )
    tool_->gaussCurvature->setEnabled(false);
}

void MeshComparePlugin::compareButton() {

  // Get source and target objects
  BaseObjectData* targetObject = 0;
  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) {

    if ( o_it->dataType(DATA_TRIANGLE_MESH)  || o_it->dataType(DATA_POLY_MESH) ) {

      // If we found a second target, something is wrong!
      if ( targetObject != 0 ) {
        emit log(LOGERR,tr("Please select one source and one target mesh to compare! Source will be the reference mesh."));
        return;
      }

      targetObject = (*o_it);
    }
  }

  BaseObjectData* sourceObject = 0;
  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::SOURCE_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) {

    if ( o_it->dataType(DATA_TRIANGLE_MESH)  || o_it->dataType(DATA_POLY_MESH) ) {

      // If we found a second target, something is wrong!
      if ( sourceObject != 0 ) {
        emit log(LOGERR,tr("Please select one source and one target mesh to compare! Source will be the reference mesh."));
        return;
      }

      sourceObject = (*o_it);
    }
  }


  if ( (targetObject != 0) && (sourceObject != 0)  ) {
    compare(sourceObject->id(),targetObject->id(), tool_->distance->isChecked() ,
                                                   tool_->normalAngle->isChecked(),
                                                   tool_->gaussCurvature->isChecked(),
                                                   tool_->meanCurvature->isChecked(),
                                                   tool_->selection->isChecked());
  } else {
    emit log(LOGERR,tr("Please select one source and one target mesh to compare! Source will be the reference mesh."));
  }


}

void MeshComparePlugin::slotObjectUpdated( int _identifier, const UpdateType& _type ) {
  // Get source and target objects
  BaseObjectData* object = 0;

  PluginFunctions::getObject(_identifier,object);

  if ( object ) {
    ACG::SceneGraph::MaterialNode *pMatNode = 0;
    if ( object->getAdditionalNode(pMatNode,name(), "MeshCompareDistanceMaterial" ) )
      object->removeAdditionalNode(pMatNode,name(),"MeshCompareDistanceMaterial");
  }

}

void MeshComparePlugin::slotClear() {

  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::ALL_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) {

    ACG::SceneGraph::MaterialNode *pMatNode = 0;
    if ( o_it->getAdditionalNode(pMatNode,name(), "MeshCompareDistanceMaterial" ) )
      o_it->removeAdditionalNode(pMatNode,name(),"MeshCompareDistanceMaterial");

  }

  emit updateView();

}

void MeshComparePlugin::slotClampBox(bool _checked) {
  if ( _checked ) {
    tool_->minVal->setValue(tool_->minValue->text().toDouble());
    tool_->maxVal->setValue(tool_->maxValue->text().toDouble());
  }
}

void MeshComparePlugin::compare(int _sourceId,int _targetId,bool _computeDist, bool _computeNormal, bool _computeGauss , bool _computeMean, bool _selection) {

  TriMeshObject* source = PluginFunctions::triMeshObject(_sourceId);
  TriMeshObject* target = PluginFunctions::triMeshObject(_targetId);

  if ( (target == 0 ) || (source == 0) ) {
    emit log(LOGERR,tr("Please select one source and one target mesh to compare! Source will be the reference mesh."));
    emit log(LOGERR,tr("Only triangle meshes are currently supported!"));
    return;
  }

  TriMesh* refMesh  = PluginFunctions::triMesh(_sourceId);
  TriMesh* compMesh = PluginFunctions::triMesh(_targetId);

  ACG::SceneGraph::PointNode *pNode = 0;

  // Check if we already attached a PointNode
  if ( OpenFlipper::Options::gui() ) {

    if ( !target->getAdditionalNode(pNode,name(), "MeshCompareDistance" ) ) {

      ACG::SceneGraph::MaterialNode *pMatNode = 0;
      pMatNode = new ACG::SceneGraph::MaterialNode(target->manipulatorNode(), "MeshCompare distance visualization material");
      target->addAdditionalNode(pMatNode, name(), "MeshCompareDistanceMaterial");
      pMatNode->set_point_size(5);
      pMatNode->applyProperties(ACG::SceneGraph::MaterialNode::PointSize);

      pNode = new ACG::SceneGraph::PointNode(pMatNode, "MeshCompare distance visualization");
      pNode->drawMode(ACG::SceneGraph::DrawModes::POINTS_COLORED);
      target->addAdditionalNode(pNode, name(), "MeshCompareDistance");
    }

    // Clear but reserve memory for the required vertices
    pNode->clear();
    pNode->reserve(refMesh->n_vertices(),refMesh->n_vertices(),refMesh->n_vertices() );
  }

  // Get a bsp for the target, as we will project the reference mesh onto the target mesh.
  // It will be build automatically at this point.
  TriMeshObject::OMTriangleBSP* compBSP = target->requestTriangleBsp();

  // ================================================================
  // Compute mean curvature on both meshes ( if plugin is available )
  // ================================================================
  bool meanCurvature = false;
  OpenMesh::VPropHandleT< double > meanRef;
  OpenMesh::VPropHandleT< double > meanComp;

  if ( _computeMean ) {
    emit pluginExists( "meancurvature" , meanCurvature  );

    if ( meanCurvature ) {
      RPC::callFunction("meancurvature","computeMeanCurvature",_sourceId);
      RPC::callFunction("meancurvature","computeMeanCurvature",_targetId);
    }

    if( meanCurvature &&
        ((!refMesh->get_property_handle(  meanRef , "Mean Curvature") ) ||
            (!compMesh->get_property_handle( meanComp, "Mean Curvature") ))) {
      meanCurvature = false;
    }
  }

  // ================================================================
  // Compute mean curvature on both meshes ( if plugin is available )
  // ================================================================
  bool gaussCurvature = false;
  OpenMesh::VPropHandleT< double > gaussRef;
  OpenMesh::VPropHandleT< double > gaussComp;

  if ( _computeGauss ) {
    emit pluginExists( "gausscurvature" , gaussCurvature  );

    if ( gaussCurvature ) {
      RPC::callFunction("gausscurvature","computeGaussCurvature",_sourceId);
      RPC::callFunction("gausscurvature","computeGaussCurvature",_targetId);
    }

    if( gaussCurvature &&
        ((!refMesh->get_property_handle(  gaussRef , "Gaussian Curvature") ) ||
            (!compMesh->get_property_handle( gaussComp, "Gaussian Curvature") ))) {
      gaussCurvature = false;
    }
  }



  // ================================================================
  // Remember the maximal values as output and for specifying color coding range
  // ================================================================
  maximalDistance_      = -1.0;
  maxNormalDeviation_   = -1.0;
  maxMeanCurvatureDev_  = -1.0;
  maxGaussCurvatureDev_ = -1.0;


  // Remember distances for colorCoding after we know the maximal distance
  std::vector<double> distances;
  std::vector<double> normalAngles;
  std::vector<double> meanCurvatures;
  std::vector<double> gaussCurvatures;


  for (auto v_it : refMesh->vertices()) {
    if ( _selection && refMesh->status(v_it).selected() == false) {
      continue;
    }

    TriMeshObject::OMTriangleBSP::NearestNeighbor nearest = compBSP->nearest(refMesh->point(v_it));
    TriMesh::FaceHandle closestFace = nearest.handle;

    // Remember the maximal distance between the meshes
    if (nearest.dist > maximalDistance_)
      maximalDistance_ = nearest.dist;

    // Remember distance for color coding
    distances.push_back(nearest.dist);

    // Get the vertices around that face and their properties
    TriMesh::CFVIter fv_it = compMesh->cfv_iter(closestFace);

    const TriMesh::Point& p0 = compMesh->point(*fv_it);
    const TriMesh::Normal n0 = compMesh->normal(*fv_it);
    const TriMesh::VertexHandle& v0 = *fv_it;

    const TriMesh::Point& p1 = compMesh->point(*(++fv_it));
    const TriMesh::Normal n1 = compMesh->normal(*fv_it);
    const TriMesh::VertexHandle& v1 = *fv_it;

    const TriMesh::Point& p2 = compMesh->point(*(++fv_it));
    const TriMesh::Normal n2 = compMesh->normal(*fv_it);
    const TriMesh::VertexHandle& v2 = *fv_it;

    // project original point to current mesh
    TriMesh::Point projectedPoint;
    ACG::Geometry::distPointTriangle(refMesh->point(v_it), p0, p1, p2, projectedPoint);

    // Add the position to the point node
    if (pNode)
      pNode->add_point(projectedPoint);

    // compute Barycentric coordinates
    ACG::Geometry::baryCoord(projectedPoint, p0, p1, p2, projectedPoint);

    if ( _computeNormal) {
      // interpolate normal on the compare mesh at the projected point via barycentric coordinates.
      TriMesh::Normal normal;
      normal = n0 * projectedPoint[0];
      normal += n1 * projectedPoint[1];
      normal += n2 * projectedPoint[2];
      normal.normalize();

      // Compute normal deviation in degrees
      double normalDeviation = (refMesh->normal(v_it) | normal);

      if (normalDeviation < -1.0)
        normalDeviation = -1.0;
      else if (normalDeviation > 1.0)
        normalDeviation = 1.0;

      normalDeviation = 180.0 / M_PI * acos(normalDeviation);

      // Remember normal deviation for color coding
      normalAngles.push_back(normalDeviation);

      if (normalDeviation > maxNormalDeviation_)
        maxNormalDeviation_ = normalDeviation;
    }

    if (meanCurvature) {

      TriMesh::Scalar curvature =  compMesh->property(meanComp, v0) * projectedPoint[0] +
                                   compMesh->property(meanComp, v1) * projectedPoint[1] +
                                   compMesh->property(meanComp, v2) * projectedPoint[2];

      const double curvatureDev = fabs(refMesh->property(meanRef, v_it) - curvature);

      meanCurvatures.push_back(curvatureDev);

      if (curvatureDev > maxMeanCurvatureDev_)
        maxMeanCurvatureDev_ = curvatureDev;
    }

    if (gaussCurvature) {

      TriMesh::Scalar curvature = compMesh->property(gaussComp, v0) * projectedPoint[0] +
                                  compMesh->property(gaussComp, v1) * projectedPoint[1] +
                                  compMesh->property(gaussComp, v2) * projectedPoint[2];

      const double curvatureDev = fabs(refMesh->property(gaussRef, v_it) - curvature);

      gaussCurvatures.push_back(curvatureDev);

      if (curvatureDev > maxGaussCurvatureDev_)
        maxGaussCurvatureDev_ = curvatureDev;
    }

  }

  // Generate the colors
  if ( pNode ) {

    tool_->minValue->setText( QString::number(0.0) );

    if ( tool_->distance->isChecked() ) {
      visualizeData(distances,maximalDistance_,pNode);
    } else if ( tool_->normalAngle->isChecked() ) {
      visualizeData(normalAngles,maxNormalDeviation_,pNode);
    } else if ( tool_->meanCurvature->isChecked() ) {
      visualizeData(meanCurvatures,maxMeanCurvatureDev_,pNode);
    } else if ( tool_->gaussCurvature->isChecked() ) {
      visualizeData(gaussCurvatures,maxGaussCurvatureDev_,pNode);
    }

    emit updateView();
  }

}

void MeshComparePlugin::visualizeData( const std::vector<double>& _data, double _maxValue, ACG::SceneGraph::PointNode* _pnode ) {

  // Set the current real maximal value in the label to show it to the user
  tool_->maxValue->setText( QString::number(_maxValue) );

  // If the clamping check box is set, we take the values from the spin boxes
  double min = 0.0;
  double max = 1.0;
  if ( tool_->doClamp->isChecked() ) {
    min = tool_->minVal->value();
    max = std::min(tool_->maxVal->value(),_maxValue);
  } else
    max = _maxValue;

  ACG::ColorCoder cCoder(min,max);

  for ( unsigned int i = 0 ; i < _data.size() ; ++i) {
    _pnode->add_color(cCoder.color_float4(_data[i]));
  }

  plot_->setMinMax(min,max);
  plot_->setFunction( _data );
  plot_->replot();

}

