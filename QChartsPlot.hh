/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


//=============================================================================
//
// CLASS QChartsPlot
//
//
// Author:  David Bommes <bommes@cs.rwth-aachen.de>
//
// Version: $Revision: 1$
// Date:    $Date: 200X-XX-XX$
//
//=============================================================================


#pragma once

//== INCLUDES =================================================================

#include "QChartView"
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QRubberBand>
#include <QStackedBarSeries>
#include <QBarSet>


#include <ui_QChartsPlotBase.h>

#include <vector>

#include <QDialog>

//== FORWARDDECLARATIONS ======================================================

//== NAMESPACES ===============================================================

//== CLASS DEFINITION =========================================================

class QChartsPlot :  public QChartView, public Ui::QChartsPlotBase
{
  Q_OBJECT
public:

  /// Default constructor
  explicit QChartsPlot( QWidget*    _parent = 0 );

  /// Destructor
  ~QChartsPlot() {}

  bool eventFilter(QObject *obj, QEvent *event) override;

  /// set the function to plot
  void setFunction(const std::vector<double>& _values );

  void setMinMax(double _min, double _max);

  void replot();


public slots:

  void rubberBandChangedSlot(double rubber_min, double rubber_max);

signals:  

  void rubberBandChanged(double rubber_min, double rubber_max);

private:

  /// The Rubberband
  QRubberBand* rubberBand_;

  /// The chart
  QChart* chart_;

  /// Values that will define the function to plot
  std::vector<double> values_;

  double min_,max_;

  double yMin_, yMax_;

  QStackedBarSeries* series;

  QValueAxis* axisY;

  QValueAxis* axisX;

protected:

  void mouseReleaseEvent(QMouseEvent* event) override;
};


//=============================================================================
//=============================================================================


